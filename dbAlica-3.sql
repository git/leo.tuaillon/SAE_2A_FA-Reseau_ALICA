-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : lun. 20 nov. 2023 à 20:05
-- Version du serveur : 5.7.39
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dbAlica`
--

-- --------------------------------------------------------

--
-- Structure de la table `Alumni`
--

CREATE TABLE `Alumni` (
  `id` int(11) NOT NULL,
  `mail` varchar(128) NOT NULL,
  `mdp` varchar(256) NOT NULL,
  `role` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Alumni`
--

INSERT INTO `Alumni` (`id`, `mail`, `mdp`, `role`) VALUES
(1, 'test', 'test', 'admin'),
(4, 'Admin@alica.com', '$2y$10$f2z.qWTtGhp.nZo0zBlw8Og9GpcQYcBOTIfqAj3UP2MEiN5uXS1Ue', 'Admin'),
(5, 'john@doe', '$2y$10$oOMH01Zxkz4yQPVs44fkHODMc78m8eeIOaSMF84K1w4ikPyUAiwwy', 'Membre'),
(6, 'jack@doe', '$2y$10$sFeUX9.evOansuqwj4nFuOLy9n3j6tkAFHmsL1kTDuUxhgF6WZRZy', 'Membre'),
(7, 'test@gmail.com', '$2y$10$41F6OQz9V1cr2D1rYX9np.5fTKe68dYrJlLpZf7t5G9c8g2mgOF9K', 'Membre'),
(8, 'admin@test', '$2y$10$mHKhDhpN7.Z1UyvE3..ZIuGPbjtmUa9QDhmuQyU1h68d/2z25DUDK', 'Admin'),
(9, 'membre@test', '$2y$10$hVS.BcHq.b/oneN0KiJI3u6CXGeb2UXRw5mJtvZByJG1./mIG4QbK', 'Membre'),
(10, 'test@e', '$2y$10$K3LBNsukXWvBqsbEid5Px.PnNQmomNa0tlnNvFlxDxRYdSbfOWdcK', 'Membre'),
(11, 'emma.dupont@email.com', '$2y$10$eg7ARpvdTqnpJ1tMi.AhcufFZtBQxkhnE8GF1/z4RRrTg3C1wpCsC', 'Membre'),
(12, 'lucas.martin@email.com', '$2y$10$yk7/MCHif6niCjqWrFxA2u1C69ThVb9tpduyTZtcDjnW.V.gM51Iu', 'Membre'),
(13, 'chloe.bernard@mail', '$2y$10$p0BjmKiuFbd3zBzBo6sWJ.tZi./2J7c3MZ9unCn/OkW6LBBvnC.FC', 'Membre'),
(14, 'ines.leroy@email.com', '$2y$10$QDoHq2B7m6WseAPe6kH6MuoaCREBWqChO95NxUVMrZDH1aShHD7zW', 'Membre'),
(15, 'te@te', '$2y$10$.kM.SFMvZnDEGuE8rxclc.UaJbKYesqzNNkE5VdE7NAPEwWOTS4u6', 'Membre'),
(16, 'lea.simon@email.com', '$2y$10$AckHtFDM3C.O.ACQlqh5FuVqSr3bB2.IjcPmdrrM/fYRYU6lU6OdW', 'Membre'),
(17, 'gabriel.michel@email.com', '$2y$10$bNoO9pNylyCL2hznrZ7OyO7aWVJUrZieda2uwnYo/Ly8TB7Nl7fLS', 'Membre'),
(18, 'Sarah@email', '$2y$10$50o/3cFzapPB90apopD5b.tWrRxCG0sYOni6TinI41cwHCzYFiUFO', 'Membre'),
(19, 'raphael.garcia@email.fr', '$2y$10$m2dHfABuizTRBGymKfO60eyzo13BPTSSH/EdEhXcDkauzuRuk7HR.', 'Membre'),
(20, 'Alice@a', '$2y$10$atRfojz7W0mQzv9l0IgRhOF5WWn7FjTRZtSbZkzQveYdInMsy5RrS', 'Membre'),
(21, 'maxime.pichon@email.com', '$2y$10$sA3N6D01ATFdCKqJ6fflNe484KMKejFIfaTYHGOH9AoPHOnRmSvi2', 'Membre'),
(22, 'zoe.Gauthier@gmail.com', '$2y$10$bVaPrdlcYOHcclq0OjZUTOwoLkmMXLw9Ww6OTSDnhFAujDL4nqBqG', 'Membre');

-- --------------------------------------------------------

--
-- Structure de la table `Article`
--

CREATE TABLE `Article` (
  `id` int(11) NOT NULL,
  `auteur` int(11) NOT NULL,
  `Titre` varchar(64) NOT NULL,
  `sousTitre` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `image` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Evenement`
--

CREATE TABLE `Evenement` (
  `id` int(11) NOT NULL,
  `organisateur` int(11) NOT NULL,
  `titre` varchar(64) NOT NULL,
  `description` varchar(512) NOT NULL,
  `image` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `nbPlaceMax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Evenement`
--

INSERT INTO `Evenement` (`id`, `organisateur`, `titre`, `description`, `image`, `date`, `nbPlaceMax`) VALUES
(1, 1, 'Laser Game', 'Evement laser game avec tous les membres du réseau ALICA, vous êtes la bienvenue..', '18044Laser Game PBOB9302.jpg', '2023-11-18', 20),
(2, 1, 'Rencontre Annuelle des Anciens d\'Alica', 'Un rassemblement annuel pour tous les anciens étudiants, avec des discours inspirants, des ateliers de réseautage et un dîner de gala\r\nLieu : Centre de conférence de l\'IUT d’Aubière', '16494Bandeau CreerAsso.jpg', '2023-11-24', 30);

-- --------------------------------------------------------

--
-- Structure de la table `Experience`
--

CREATE TABLE `Experience` (
  `id` int(11) NOT NULL,
  `profil` int(11) NOT NULL,
  `intitule` varchar(256) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `nomEntreprise` varchar(64) NOT NULL,
  `currentJob` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Formation`
--

CREATE TABLE `Formation` (
  `id` int(11) NOT NULL,
  `profil` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `ville` varchar(32) NOT NULL,
  `dateDeb` date NOT NULL,
  `dateFin` date NOT NULL,
  `currentFormation` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Image`
--

CREATE TABLE `Image` (
  `id` int(5) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `taille` varchar(25) NOT NULL,
  `type` varchar(25) NOT NULL,
  `blob` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Offre`
--

CREATE TABLE `Offre` (
  `id` int(11) NOT NULL,
  `offreur` int(11) NOT NULL,
  `titre` varchar(128) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `image` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `typeContrat` varchar(16) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `entreprise` varchar(64) NOT NULL,
  `descriptifPoste` varchar(2028) NOT NULL,
  `profil` varchar(2028) NOT NULL,
  `experience` varchar(512) NOT NULL,
  `niveauEtudes` varchar(16) NOT NULL,
  `mailContact` varchar(128) NOT NULL,
  `numero` varchar(12) NOT NULL,
  `websiteURL` varchar(256) NOT NULL,
  `remote` tinyint(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Participer`
--

CREATE TABLE `Participer` (
  `alumni` int(11) NOT NULL,
  `evenement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Profil`
--

CREATE TABLE `Profil` (
  `id` int(11) NOT NULL,
  `alumni` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `cv` varchar(256) DEFAULT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `linkedinURL` varchar(256) DEFAULT NULL,
  `githubURL` varchar(256) DEFAULT NULL,
  `portfolioURL` varchar(256) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Profil`
--

INSERT INTO `Profil` (`id`, `alumni`, `email`, `cv`, `nom`, `prenom`, `linkedinURL`, `githubURL`, `portfolioURL`, `image`) VALUES
(4, 4, 'Admin@alica.com', NULL, 'TUAILLON', 'Leo', NULL, NULL, NULL, '19879.png'),
(5, 5, 'john@doe', NULL, 'Doe', 'John', NULL, NULL, NULL, '2.jpg'),
(6, 6, 'jack@doe', NULL, 'Doe', 'Jack', NULL, NULL, NULL, NULL),
(7, 7, 'test@gmail.com', NULL, 'test', 'test', NULL, NULL, NULL, NULL),
(8, 8, 'admin@test', NULL, 'Admin', 'CompteTest', NULL, NULL, NULL, '1.png'),
(9, 9, 'membre@test', NULL, 'Membre', 'compteTest', NULL, NULL, NULL, '3.jpg'),
(10, 10, 'test@e', NULL, 'nom', 'prénom', NULL, NULL, NULL, NULL),
(11, 11, 'emma.dupont@email.com', NULL, 'Dupont', 'Emma', NULL, NULL, NULL, NULL),
(12, 12, 'lucas.martin@email.com', NULL, 'Martin', 'Lucas', NULL, NULL, NULL, NULL),
(13, 13, 'chloe.bernard@mail', NULL, 'Bernard', 'Chloé', NULL, NULL, NULL, '1.png'),
(14, 14, 'ines.leroy@email.com', NULL, 'Leroy', 'Ines', NULL, NULL, NULL, NULL),
(15, 15, 'te@te', NULL, 'Moreau', 'Alexandre', NULL, NULL, NULL, NULL),
(16, 16, 'lea.simon@email.com', NULL, 'Simon', 'Léa', NULL, NULL, NULL, '3.jpg'),
(17, 17, 'gabriel.michel@email.com', NULL, 'Michel', 'Gabriel', NULL, NULL, NULL, NULL),
(18, 18, 'Sarah@email', NULL, 'Lefevre', 'Sarah', NULL, NULL, NULL, NULL),
(19, 19, 'raphael.garcia@email.fr', NULL, 'Garcia', 'Raphaël', NULL, NULL, NULL, NULL),
(20, 20, 'Alice@a', NULL, 'Alice', 'Alice', NULL, NULL, NULL, '2.jpg'),
(21, 21, 'maxime.pichon@email.com', NULL, 'Pichon', 'Maxime', NULL, NULL, NULL, NULL),
(22, 22, 'zoe.Gauthier@gmail.com', NULL, 'Gauthier', 'zoé', NULL, NULL, NULL, '3.jpg');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Alumni`
--
ALTER TABLE `Alumni`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Index pour la table `Article`
--
ALTER TABLE `Article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image` (`image`),
  ADD KEY `auteur` (`auteur`);

--
-- Index pour la table `Evenement`
--
ALTER TABLE `Evenement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organisateur` (`organisateur`);

--
-- Index pour la table `Experience`
--
ALTER TABLE `Experience`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Formation`
--
ALTER TABLE `Formation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Image`
--
ALTER TABLE `Image`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Offre`
--
ALTER TABLE `Offre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offreur` (`offreur`);

--
-- Index pour la table `Profil`
--
ALTER TABLE `Profil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alumni` (`alumni`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Alumni`
--
ALTER TABLE `Alumni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `Article`
--
ALTER TABLE `Article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Evenement`
--
ALTER TABLE `Evenement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Experience`
--
ALTER TABLE `Experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Formation`
--
ALTER TABLE `Formation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Image`
--
ALTER TABLE `Image`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Offre`
--
ALTER TABLE `Offre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Profil`
--
ALTER TABLE `Profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Article`
--
ALTER TABLE `Article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`image`) REFERENCES `Image` (`id`),
  ADD CONSTRAINT `article_ibfk_2` FOREIGN KEY (`auteur`) REFERENCES `Alumni` (`id`);

--
-- Contraintes pour la table `Evenement`
--
ALTER TABLE `Evenement`
  ADD CONSTRAINT `evenement_ibfk_1` FOREIGN KEY (`organisateur`) REFERENCES `Alumni` (`id`);

--
-- Contraintes pour la table `Offre`
--
ALTER TABLE `Offre`
  ADD CONSTRAINT `offre_ibfk_1` FOREIGN KEY (`offreur`) REFERENCES `Alumni` (`id`);

--
-- Contraintes pour la table `Profil`
--
ALTER TABLE `Profil`
  ADD CONSTRAINT `fk_profil_alumni` FOREIGN KEY (`alumni`) REFERENCES `Alumni` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
