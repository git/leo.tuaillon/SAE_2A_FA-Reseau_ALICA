<?php
/** PC IUT - PHP 8.1 */

/** Chargement config */

require_once __DIR__ . '/src/config/config.php';
require __DIR__ . '/vendor/autoload.php';


/** Configuration twig */
$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
    'debug' => true
]);
$twig->addExtension(new \Twig\Extension\DebugExtension());
$twig->addGlobal('dir', '/SAE_2A_FA-Reseau_ALICA/php');




$cont = new \App\controleur\FrontControleur();
