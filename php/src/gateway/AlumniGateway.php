<?php
namespace App\gateway;
use PDO;
use App\metier\Alumni;
use App\metier\Role;
use App\metier\Profil;

class AlumniGateway
{
    private Connection $con;

    /**
     * @param $con
     */
    public function __construct(Connection $con)
    {
        $this->con = $con;
    }

    public function insert(string $email, string $motDePasse, string $role)
    {
        $query = 'INSERT INTO Alumni (mail, mdp, role) VALUES (:mail, :mdp, :role)';
        return $this->con->executeQuery($query, array(
            ':mail' => array($email, PDO::PARAM_STR),
            ':mdp' => array($motDePasse, PDO::PARAM_STR),
            ':role' => array($role, PDO::PARAM_STR)
        ));
    }


    public function updateMotDePasse(int $id, string $password)
    {
        $query = 'UPDATE Alumni SET motDePasse=:new WHERE id=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($id, PDO::PARAM_INT),
            ':new' => array($password, PDO::PARAM_STR)
        ));
    }

    public function ObtenirById(int $id): array
    {
        $query = 'SELECT * FROM Alumni WHERE id=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($id, PDO::PARAM_INT)
        ));
        return $this->con->getResults();
    }

    public function findByEmail(string $email)
    {
        $query = 'SELECT Alumni.id, Alumni.mail, Alumni.mdp, Alumni.role, Profil.nom, Profil.prenom
              FROM Alumni 
              LEFT JOIN Profil ON Alumni.id = Profil.alumni
              WHERE Alumni.mail = :e';
        $this->con->executeQuery($query, array(
            ':e' => array($email, PDO::PARAM_STR),
        ));
        return $this->con->getResults();
    }


    public function getAll()
    {
        $query = 'SELECT * FROM Alumni';
        $this->con->executeQuery($query);
        return $this->con->getResults();
    }

    public function getID(string $email)
    {
        $query = 'SELECT id FROM Alumni WHERE mail=:e';
        $this->con->executeQuery($query, array(
            ':e' => array($email, PDO::PARAM_STR),
        ));
        $res = $this->con->getResults();
        return $res[0]['id'];
    }

    public function deleteUser(int $accountId)
    {
        // Supprimer les enregistrements liés dans AutreTable
        $query = 'DELETE FROM Profil WHERE alumni=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($accountId, PDO::PARAM_INT)
        ));

        $query = 'DELETE FROM Offre WHERE offreur=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($accountId, PDO::PARAM_INT)
        ));

        // Ensuite, supprimer l'enregistrement dans Alumni
        $query = 'DELETE FROM Alumni WHERE id=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($accountId, PDO::PARAM_INT)
        ));

        return $this->con->getResults();
    }

}
