<?php
namespace App\gateway;
use App\metier\Evenement;

class EvenementGateway
{
    private \App\gateway\Connection $con;

    /**
     * @param $con
     */
    public function __construct(\App\gateway\Connection $con){
        $this->con = $con;
    }

    public function getNewId()
    {
        $query='SELECT MAX(id) FROM Evenement';
        $this->con->executeQuery($query);
        $res=$this->con->getResults();
        return $res[0]['MAX(id)']+1;
    }

    public function insertEvenement(Evenement $evenement)
    {

        $query = 'INSERT INTO Evenement VALUES (:i, :o, :t, :d, :img, :date, :nb)';

        $this->con->executeQuery($query,$params = array(
            ':i'    => array($evenement->getId(), \PDO::PARAM_INT),
            ':o'    => array($evenement->getOrganisateurId(), \PDO::PARAM_INT),
            ':t'    => array($evenement->getTitre(), \PDO::PARAM_STR),
            ':d'    => array($evenement->getDescription(), \PDO::PARAM_STR),
            ':img'  => array($evenement->getImage(), \PDO::PARAM_STR),
            ':date' => array($evenement->getDate(), \PDO::PARAM_STR),
            ':nb'   => array($evenement->getNbPlaceMax(), \PDO::PARAM_INT),
        ));
    }


    public function getAllEvenement()
    {
        $query = 'SELECT * FROM Evenement ORDER BY id DESC';
        $this->con->executeQuery($query);
        $res = $this->con->getResults();
        return $res;
    }

    public function deleteEvenement($id)
    {
        $query='DELETE FROM Evenement WHERE id=:id';
        $this->con->executeQuery($query, array(
            ':id' => array($id, \PDO::PARAM_INT)
        ));
    }

    public function findById($id)
    {
        $query='SELECT * FROM Evenement WHERE id=:id';
        $this->con->executeQuery($query, array(
            ':id' => array($id, \PDO::PARAM_INT)
        ));
        $res=$this->con->getResults();
        return $res;
    }

    public function findByTitle($titre)
    {
        $query = 'SELECT * FROM Evenement WHERE titre LIKE :titre';
        $this->con->executeQuery($query, array(
            ':titre' => array("%$titre%", \PDO::PARAM_STR)
        ));
        $res = $this->con->getResults();
        return $res;
    }

}