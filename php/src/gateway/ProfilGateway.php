<?php

namespace App\gateway;
use PDO;
class ProfilGateway
{
    private Connection $con;
    /**
     * @param Connection $con
     */
    public function __construct(Connection $con)
    {
        $this->con = $con;
    }

    public function insert(int $alumni,string $nom, string $prenom, string $email)
    {
        $query = 'INSERT INTO Profil (alumni,nom, prenom,email) VALUES (:alumni,:nom, :prenom,:email)';
        return $this->con->executeQuery($query, array(
            ':alumni' => array($alumni, PDO::PARAM_INT),
            ':nom' => array($nom, PDO::PARAM_STR),
            ':prenom' => array($prenom, PDO::PARAM_STR),
            ':email' => array($email, PDO::PARAM_STR)
        ));
    }


    public function getProfilById($id)
    {
        $query = 'SELECT * FROM Profil WHERE Alumni=:i';
        $this->con->executeQuery($query, array(
            ':i' => array($id, PDO::PARAM_INT)
        ));
        return $this->con->getResults();
    }

    public function userByPage(int $page, int $nbParPage)
    {
        $start = ($page - 1) * $nbParPage; // Calcul de l'index de départ pour LIMIT
        $start = max(0, $start); // Si $start est négatif, on met 0 (pas de page -1)
        $query = 'SELECT * FROM Profil LIMIT :start, :nbParPage';
        $this->con->executeQuery($query, array(
            ':start' => array($start, PDO::PARAM_INT),
            ':nbParPage' => array($nbParPage, PDO::PARAM_INT),
        ));
        return $this->con->getResults();
    }
}