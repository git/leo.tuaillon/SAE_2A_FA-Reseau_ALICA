<?php
namespace App\gateway;

use App\metier\Alumni;
use App\metier\Offre;
class OffreGateway
{
    private \App\gateway\Connection $con;

    /**
     * @param $con
     */
    public function __construct(\App\gateway\Connection $con){
        $this->con = $con;
    }

    public function getNewId() : int
    {
        $query = 'SELECT MAX(id) FROM Offre';
        $this->con->executeQuery($query, array());
        $res = $this->con->getResults();
        if($res[0]['MAX(id)'] == null )
            return 1;
        else
        return intval($res[0]['MAX(id)'])+1;
    }

    public function getNbOffers(): int
    {
        $query = 'SELECT COUNT(*) FROM Offre';
        $this->con->executeQuery($query, array());
        $res = $this->con->getResults();
        return intval($res[0]['COUNT(*)']);
    }

    public function getOfferLimit($start, $nbOffers): array
    {
        $query = 'SELECT * FROM Offre LIMIT :s, :nb';
        $this->con->executeQuery($query, array(
            ':s' => array($start, \PDO::PARAM_INT),
            ':nb' => array($nbOffers, \PDO::PARAM_INT)
        ));

        //echo "start : " . $start . "<br> end : " . $end . "<br>";
        //echo "Number of results: " . count($this->con->getResults()) . "<br>";
        return $this->con->getResults();
    }



    public function addOffers(Offre $offre)
    {
        $query = 'INSERT INTO Offre VALUES (:i, :o, :t, :d, :img, :logo, :ty, :v, :e, :desc, :pro, :exp, :niv, :mail, :num, :web, :remote, :date)';
        $this->con->executeQuery($query, array(
            ':i' => array($offre->getId(), \PDO::PARAM_INT),
            ':o' => array($offre->getOffreurId(), \PDO::PARAM_INT),
            ':t' => array($offre->getNom(), \PDO::PARAM_STR),
            ':d' => array($offre->getDescription(), \PDO::PARAM_STR),
            'img' => array($offre->getImg(), \PDO::PARAM_STR),
            'logo' => array($offre->getLogo(), \PDO::PARAM_STR),
            ':ty' => array($offre->getTypeContrat(), \PDO::PARAM_STR),
            ':v' => array($offre->getVille(), \PDO::PARAM_STR),
            ':e' => array($offre->getEntreprise(), \PDO::PARAM_STR),
            ':desc' => array($offre->getDescriptifPoste(), \PDO::PARAM_STR),
            ':pro' => array($offre->getProfilSearched(), \PDO::PARAM_STR),
            ':exp' => array($offre->getExperience(), \PDO::PARAM_STR),
            ':niv' => array($offre->getNiveauEtudes(), \PDO::PARAM_STR),
            ':mail' => array($offre->getMailContact(), \PDO::PARAM_STR),
            ':num' => array($offre->getNumero(), \PDO::PARAM_STR),
            ':web' => array($offre->getSiteUrl(), \PDO::PARAM_STR),
            ':remote' => array($offre->isRemote(), \PDO::PARAM_BOOL),
            ':date' => array($offre->getDateString(), \PDO::PARAM_STR)
        ));
    }

    public function  getOffers() : array
    {
        $query = 'SELECT * FROM offre';
        $this->con->executeQuery($query, array());
        return $this->con->getResults();
    }

    public function getOfferFromId($id) : array
    {
        $query = "SELECT * FROM offre WHERE id=:id";
        $this->con->executeQuery($query, array(
            ':id' => array($id, \PDO::PARAM_INT)
        ));
        return $this->con->getResults();
    }

    public function getOffersWithFilters($filters) : array
    {
        $typeContrat = $filters['typeContrat'];
        $exp = $filters['exp'];
        $niveauEtudes = $filters['niveauEtudes'];

        $query = "SELECT * FROM Offre WHERE";

        $params = array();

        if ($typeContrat != null) {
            $query .= " typeContrat = :type";
            $params[':type'] = array($typeContrat, \PDO::PARAM_STR);
        }

        if ($exp != null) {
            $query .= ($typeContrat != null ? " AND" : "") . " experience = :exp";
            $params[':exp'] = array($exp, \PDO::PARAM_STR);
        }

        if ($niveauEtudes != null) {
            $query .= (($typeContrat != null || $exp != null) ? " AND" : "") . " niveauEtudes = :lvl";
            $params[':lvl'] = array($niveauEtudes, \PDO::PARAM_STR);
        }

        if(isset($filters['start']) && isset($filters['nbOffers']))
        {
            $query .= " LIMIT :s, :nb";
            $params[':s'] = array($filters['start'], \PDO::PARAM_INT);
            $params[':nb'] = array($filters['nbOffers'], \PDO::PARAM_INT);
        }


        $this->con->executeQuery($query, $params);

        return $this->con->getResults();
    }

    public function getNbTotalPages()
    {
        $query = 'SELECT COUNT(*) FROM Profil';
        $this->con->executeQuery($query, array());
        $res = $this->con->getResults();
        return intval($res[0]['COUNT(*)']);
    }

    public function deleteOffer($id)
    {
        $query = 'DELETE FROM OFFRE WHERE id=:i';
        $this->con->executeQuery($query,array(
            ':i' => array($id,\PDO::PARAM_INT)
            )
        );
    }


}