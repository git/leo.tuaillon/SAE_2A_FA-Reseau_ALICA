<?php

namespace App\gateway;
class  ImageManager
{

    /**
     * @return int id aléatoire
     * @description générer un id aléatoire
     */
    public static function getId() : int
    {
        return rand(10000,19999);
    }

    /**
     * @description sauvegarder une image
     * @param string $filename
     * @return array [bool,string] tableau de retour avec le booléen et le nom de l'image
     */
    public static function SaveImage(string $filename) : array
    {
        try {
            $return=[];

            $name = $_FILES[$filename]["name"];
            $path = "public/uploads/".$name;
            while(file_exists($path))
            {
                $name = substr($_FILES[$filename]["name"], 0, 45);
                $name = self::getId().$name;
                $path = "public/uploads/".$name;
            }
            

            move_uploaded_file($_FILES[$filename]['tmp_name'], "public/uploads/$name");
            $return[]=true;
            $return[]=$name;
            return $return;
        } catch (\Exception $e) {
            $return[] = false;
            $return[] = "";
            return $return;
        }
    }

    /**
     * @param string $img
     * @return bool true si l'image a été supprimée, false sinon
     * @description supprimer une image
     */
    public static function deleteImg(string $img) : bool
    {
        $path = "public/uploads/$img";
        if (file_exists($path)) {
            unlink($path);
            return true;
        } else {
            return false;
        }
    }
}