<?php
namespace App\config;

use App\metier\Alumni;
use App\modele\ImageModele;
use App\modele\OffreModele;

class Validation
{
    public static function val_action($action)
    {
        if (!isset($action)) {
            throw new \Exception('pas d\'action');
            //on pourrait aussi utiliser
            //$action = $_GET['action'] ?? 'no';
            // This is equivalent to:
            //$action =  if (isset($_GET['action'])) $action=$_GET['action']  else $action='no';
        }
    }

    public static function val_form(string &$nom, string &$age, &$dVueEreur)
    {
        if (!isset($nom) || $nom == '') {
            $dVueEreur[] = 'pas de nom';
            $nom         = '';
        }

        if (strlen(htmlspecialchars($nom, ENT_QUOTES) === 0)) {
            $dVueEreur[] = "testative d'injection de code (attaque sécurité)";
            $nom         = '';
        }

        if (!isset($age) || $age == '' || !filter_var($age, FILTER_VALIDATE_INT)) {
            $dVueEreur[] = "pas d'age ";
            $age         = 0;
        }
    }

    /**
     * Fonction qui nettoie une chaine de caractères
     * @param string $str
     * @return string Chaine valide
     */
    public static function nettoyerString(string $str) : string{
        $newstr = preg_replace('/\x00|<[^>]*>?/', '', $str);
        return str_replace(["'", '"'], ['&#39;', '&#34;'], $newstr);
    }

    /**
     * Fonction qui valide si un entier est positif
     * @param $int
     * @return bool
     */
    public static function validerIntPossitif($int){
        return filter_var($int, FILTER_VALIDATE_INT, array("min_range"=>1));
    }

    /**
     * Fonction qui verifie si un email est correct
     * @param string $str
     * @return bool
     */
    public static function verifierEmail(string $str):bool{
        return filter_var($str, FILTER_VALIDATE_EMAIL);
    }

    
    public static function validateImage($img) : bool
    {
        if(isset($_FILES[$img]))
        {
            $typesMime = array('image/jpeg', 'image/png','image/jpg', 'image/bmp','image/webp');
            $file = $_FILES[$img]["tmp_name"];

            $mime = mime_content_type($file);
            $maxFileSize = 10 * 1024 * 1024; // 10MB Taille max acceptée
            if (in_array($mime, $typesMime) && $_FILES[$img]["size"] <= $maxFileSize) {
                return true;
            }
        }
        return false;
    }

    public static function validerEvenement(int $idOrganisateur, string $titre, string $description, string $date, int $nbPlaceMax, string $img) : bool
    {
        if(!empty($idOrganisateur) && !empty($titre) && !empty($description) && !empty($date) && !empty($nbPlaceMax) && !empty($img))
        {
            if(self::validerIntPossitif($nbPlaceMax))
            {
                if(self::validateImage($img))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static function validateNumber($number) : bool
    {
        if(preg_match("/^[0-9]{10}$/", $number))
        {
            return true;
        }
        return false;
    }

    public static function checkUrl($url) : bool
    {
        if(preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}$/", $url))
        {
            return true;
        }
        return false;
    }

    public static function validateTypeContract($typeContrat) : bool
    {
        $contrats = ["CDI","CDD","Stage","Alternance"];
        return in_array($typeContrat,$contrats);
    }

    public static function validateExperience($exp): bool
    {
        $experiences = ["Junior","Senior","Indifférent"];
        return in_array($exp,$experiences);
    }

    public static function validateTypeStudies($level) : bool
    {
        $studies = ["Bac+2","Bac+3","Bac+5","Indifférent"];
        return in_array($level,$studies);
    }

    public static function ValidateEntry($entry, $nbChars): bool
    {

        if(!empty($entry))
        {
            return strlen($entry)>= $nbChars;
        }
        return false;


    }

    public function	isAdmin() : ?Alumni
    {
        if(isset($_SESSION['login']) && isset($_SESSION['role']))
        {
            $login = self::nettoyerString($_SESSION['login']);
            $role = self::nettoyerString($_SESSION['role']);
            return $_SESSION["utilisateur"];
        }
        return null;
    }

}
