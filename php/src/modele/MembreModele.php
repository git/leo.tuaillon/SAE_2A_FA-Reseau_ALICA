<?php

namespace App\modele;

use App\gateway\ImageManager;
use App\metier\Alumni;
use App\metier\Offre;

class MembreModele extends UtilisateurModele
{


    /**
     * @description modifier photo de profil
     */
    public function updateProfilPhoto() : bool
    {
        // TO DO
        return false;
    }

    /**
     * @description ajouter formation
     */
    public function addFormation() : bool
    {
        // TO DO
        return false;
    }


    /**
     * @description modifier formation
     */
    public function updateFormation() : bool
    {
        // TO DO
        return false;
    }

    /**
     * @description ajouter Experience
     */
    public function addExperience() : bool
    {
        // TO DO
        return false;
    }

    /**
     * @description modifier experience
     */
    public function updateExpereience() : bool
    {
        // TO DO
        return false;
    }

    /**
     * @description changer mot de passe
     * @param string $oldHash ancien hash
     * @param string $newHash nouveau hash
     */
    public function updatePasswd(string $oldHash,string $newHash) : bool
    {
        // TO DO
        return false;
    }

    /**
     * @param string $img url de l'image
     * @param string $logo url du logo
     * @description publier une offre
     */
    public function publishOffer(string $img, string $logo)
    {
        $desc = $_POST["description"];
        $descposte = $_POST["descriptPoste"];
        $nom = $_POST["name"];
        $ville = $_POST["ville"];
        $entreprise = $_POST["entreprise"];
        $profilRecherche = $_POST["profilRecherche"];
        $mail = $_POST["mail"];
        $num = $_POST["num"];
        $site = $_POST["site"];
        $exp = $_POST["choixExp"];
        $typeContrat = $_POST["typeContrat"];
        $niveauEtudes = $_POST["education"];
        $date = new \DateTime();

        if(isset($_POST["fullRemote"]))
        {
            $remote = true;
        }
        else $remote = false;

        // à la place de NULL passer id utilisateur créateur offre
        $offre = new Offre($this->offreGw->getNewId(),
            new Alumni(intval($_SESSION['id']),"test.mail@icloud.fr","",$_SESSION['role'],$_SESSION['nom'],$_SESSION['prenom']),
            $nom,
            $desc,
            $img,
            $logo,
            $typeContrat,
            $ville,
            $entreprise,
            $descposte,
            $profilRecherche,
            $exp,
            $niveauEtudes,
            $mail,
            $num,
            $site,
            $remote,
            $date);

        $this->offreGw->addOffers($offre);

        return $offre;
    }

    /**
     * @param Offre $offre offre à supprimer
     * @description supprimer une offre
     * @return void
     */
    public function deleteOffer(Offre $offre)
    {
        $this->offreGw->deleteOffer($offre->getId());
        ImageManager::deleteImg($offre->getImg());
        ImageManager::deleteImg($offre->getLogo());

    }


}