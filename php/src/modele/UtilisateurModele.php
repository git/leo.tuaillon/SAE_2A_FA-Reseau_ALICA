<?php
namespace App\modele;

use App\gateway\Connection;
use App\gateway\EvenementGateway;
use App\gateway\OffreGateway;
use App\metier\Evenement;
use App\metier\Alumni;
use App\gateway\AlumniGateway;
use App\gateway\ProfilGateway;
use App\metier\Profil;
use App\metier\Offre;

class UtilisateurModele

{
    private $con;
    protected $offreGw;
    protected $eventGw;


    public function __construct()
    {
        $this->con = new Connection(DB_HOST,DB_USER,DB_PASS);
        $this->offreGw = new OffreGateway($this->con);
        $this->eventGw = new EvenementGateway($this->con);
    }

    /**
     * @description se connecter
     * @param string email
     * @param string hash
     * @return Alumni
     */

    public function connection(string $email, string $mdp): ?Alumni
    {
        $con = new Connection(DB_HOST, DB_USER, DB_PASS);
        $gate = new AlumniGateway($con);

         // Récupation de l'utilisateur avec l'email
         $utilisateur = $gate->findByEmail($email);
         if (isset($utilisateur[0])) {
             // L'utilisateur existe, vérification du mot de passe
             if (password_verify($mdp, $utilisateur[0]['mdp'])) {
                 // Le mot de passe est correct, retournez l'utilisateur
                 return new Alumni($utilisateur[0]['id'],$utilisateur[0]['mail'], $utilisateur[0]['mdp'], $utilisateur[0]['role'],$utilisateur[0]['nom'],$utilisateur[0]['prenom']);
             } else {
                 // Le mot de passe est incorrect, renvoyez null
                 return null;
             }
         } else {
             // L'utilisateur n'existe pas, renvoyez null
             return null;
         }
     }

    /**
     * @description s'inscrire
     * @param string email
     * @param string hash
     * @param string $pseudo
     * @return \Alumni chargé
     */

    public function inscription(string $prenom, string $nom, string $email, string $hashpassword): ?Alumni
    {
        $role = "Membre";
        $con = new Connection(DB_HOST, DB_USER, DB_PASS);
        $gate = new AlumniGateway($con);
        $profilGate = new ProfilGateway($con);
        // Insérez le nouvel utilisateur dans la base de données en utilisant AlumniGateway
        if ($gate->insert($email, $hashpassword, $role)) {
            $id = $gate->getID($email);
            if ($profilGate->insert($id, $nom, $prenom, $email)) {
                // L'insertion a réussi, retournez le nouvel utilisateur
                $nouvelUtilisateur = new Alumni($id, $email, $hashpassword, $role, $nom, $prenom);
                return $nouvelUtilisateur;
            }
            return null;
        } else {
            // L'insertion a échoué, renvoyez un utilisateur vide pour indiquer l'échec
            return null;
        }
    }

    public function getUtilisateurByEmail(string $email)
    {
        $con = new Connection(DB_HOST, DB_USER, DB_PASS);
        $gate = new AlumniGateway($con);
        // Récupérez l'utilisateur avec l'email donné en utilisant AlumniGateway
        $utilisateur = $gate->findByEmail($email);
        if (isset($utilisateur[0])) {
            // L'utilisateur existe, retournez-le
            return new Alumni($utilisateur[0]['id'],$utilisateur[0]['mail'], $utilisateur[0]['mdp'], $utilisateur[0]['role'],$utilisateur[0]['nom'],$utilisateur[0]['prenom']);
        } else {
            // L'utilisateur n'existe pas, renvoyez null
            return null;
        }
    }

    public function getEvenement() : array 
    {
        $gate = new EvenementGateway($this->con);

        $data = $gate->getAllEvenement();

        $evenement = array();

        foreach ($data as $row) {
            $evenement[] = new Evenement(
                $row['id'],
                $row['organisateur'],
                $row['titre'],
                $row['description'],
                $row['date'],
                $row['nbPlaceMax'],
                $row['image']
            );
        }
        return $evenement;
    }

    public function getEvenementById(int $id) : Evenement
    {
        $gate = new EvenementGateway($this->con); 

        $data = $gate->findById($id);

        $evenement = new Evenement(
            $data[0]['id'],
            $data[0]['organisateur'],
            $data[0]['titre'],
            $data[0]['description'],
            $data[0]['date'],
            $data[0]['nbPlaceMax'],
            $data[0]['image']
        );

        return $evenement;
    }

    public function getEvenementByTitre(string $titre) : array
    {
        $gate = new EvenementGateway($this->con); 

        $data = $gate->findByTitle($titre);

        $evenement = array();

        foreach($data as $row)
        {
            $evenement[] = new Evenement(
                $row['id'],
                $row['organisateur'],
                $row['titre'],
                $row['description'],
                $row['date'],
                $row['nbPlaceMax'],
                $row['image']
            );
        }

        return $evenement;
    }

    public function getProfilByPage(string $page, int $nbParPage)
    {
        $page = max(1, intval($page));
        $con = new Connection(DB_HOST, DB_USER, DB_PASS);
        $gate = new ProfilGateway($con);
        $data = $gate->userByPage($page, $nbParPage);
        if(isset($data)){
            //Création d'une liste d'objets utilisateurs
            $profils = array();
            foreach ($data as $row) {
                $profils[] = new Profil(
                    $row['alumni'],
                    $row['nom'],
                    $row['prenom'],
                    $row['email'],
                    $row['image'] ?? null,
                    $row['cv'] ?? '',
                    $row['linkedinURL'] ?? '',
                    $row['githubURL'] ?? '',
                    $row['portfolioURL'] ?? ''
                );
            }
            return $profils;
        }
        return null;
    }


    /**
     * @param int $id identifiant d'offre
     * @return Offre|null offre trouvée
     * @description récupérer une offre depuis son id
     */
    public function getOfferFromId(int $id) : ?Offre
    {
        $res = $this->offreGw->getOfferFromId($id);
        if($res != null)
            return $this->CreateOffersFromGw($res)[0];
        return null;
    }




    /**
     * @description créer une offre depuis un tableau de retour de Gw
     * @param array $res tableau de retour de Gw
     * @return array tableau d'offres
     */
    public function CreateOffersFromGw($res) : array
    {
        $alGw = new AlumniGateway(new Connection(DB_HOST,DB_USER,DB_PASS));

        $offers=[];
        foreach ($res as $row)
        {
            $resal = $alGw->ObtenirById($row['offreur']);
            
            $profilGw = new ProfilGateway(new Connection(DB_HOST,DB_USER,DB_PASS));
            $resProfl = $profilGw->getProfilById($resal[0]['id']);

            $alumni = new Alumni(intval($resal[0]['id']),$resal[0]['mail'],$resal[0]['mdp'],$resal[0]['role'],$resProfl[0]['nom'],$resProfl[0]["prenom"]);

            $date = \DateTime::createFromFormat('Y-m-d', $row['date']);

            $offers[]=new Offre(
                $row['id'],
                $alumni,
                $row['titre'],
                $row['description'],
                $row["image"],
                $row["logo"],
                $row['typeContrat'],
                $row['ville'],
                $row["entreprise"],
                $row['descriptifPoste'],
                $row['profil'],
                $row['experience'],
                $row['niveauEtudes'],
                $row['mailContact'],
                $row['numero'],
                $row['websiteURL'],
                $row['remote'],
                $date);
        }


        return $offers;
    }

    /**
     * @param $start int de départ pour la reqûete sql
     * @param $nbOffers nombre d'offres à récupérer
     * @return array tableau d'offres
     */
    public function getOfferLimit($start, $nbOffers): array
    {
        $res = $this->offreGw->getOfferLimit($start, $nbOffers);
        return $this->CreateOffersFromGw($res);
    }

    /**
     * @return int nombre d'offres
     */
    public function getNbOffers() : int
    {
        return $this->offreGw->getNbOffers();
    }

    /**
     * @param $params array filtres de la recherche
     * @return array tableau d'offres
     */
    public function getOffersWithFilters($params) : array
    {
        return $this->CreateOffersFromGw($this->offreGw->getOffersWithFilters($params));
    }

    /**
     * @return array tableau de toutes les offres
     */
    public function getOffers() : array
    {
        $res = $this->offreGw->getOffers();
        $offers = $this->CreateOffersFromGw($res);
        return $offers;
    }

    public function getNbTotalPages()
    {
        return $this->offreGw->getNbTotalPages();
    }
}
