<?php

namespace App\modele;

use App\gateway\Connection;
use App\metier\Alumni;
use App\metier\Evenement;

class AdminModele extends MembreModele
{
    /**
     * @description  supprimer un compte
     * @param Alumni $account compte à supprimer
     */
    public function deleteAccount(int $accountId)
    {
        global $twig;
        $dVueErreur = [];
        $con = new Connection(DB_HOST, DB_USER, DB_PASS);
        $gate = new \App\gateway\AlumniGateway($con);
        $result = $gate->deleteUser($accountId);
       /* try {

            var_dump($result);
        } catch (\Exception $e) {
            $dVueErreur[] = "Erreur lors de la suppression du compte";
            echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur]);
        }*/

    }

    /**
     * @description obtenir la liste des signalements
     * @return array des signalements
     */
    public function LoadReports() : array
    {
        // TO DO
        return [];
    }

    /**
     * @description supprimer une offre spécifique
     * @param Offre $offer offre à supprimer
     */

    /**
     * @description créer un évènement
     * @return \Evenement évènement créé
     */
    public function ajouterEvenement(int $idOrganisateur, string $titre, string $description, string $date, int $nbPlaceMax, string $img)
    {
        $evenement = new Evenement(
            $this->eventGw->getNewId(),
            $idOrganisateur,
            $titre,
            $description,
            $date,
            $nbPlaceMax,
            $img
        );

        $this->eventGw->insertEvenement($evenement);
    }

    /**
     * @description suppression d'un évènement
     * @param \Evenement $event évènement à supprimer
     */
    public function deleteEvenement(int $id)
    {
        $this->eventGw->deleteEvenement($id);
    }

    /**
     * @description obtenir la liste de tous les utilisateurs
     * @return array liste de tous les utilisateurs
     */
    public function getUserList() : array
    {
        // TO DO
        return [];
    }



}