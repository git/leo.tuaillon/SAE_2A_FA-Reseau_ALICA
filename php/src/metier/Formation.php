<?php
namespace App\metier;

class Formation
{
    /**
     * @var int Identifiant
     */
    private int $id;

    /**
     * @var Profil profil
     */
    private Profil $profil;

    /**
     * @var string Nom
     */
    private string $nom;

    /**
     * @var string Ville
     */
    private string $ville;

    /**
     * @var string Date début
     */
    private string $dateDebut;

    /**
     * @var string Date fin
     */
    private string $dateFin;

    /**
     * @var bool Formation Actuelle
     */
    private bool $formationActuelle;

    /**
     * @param int $id
     * @param Profil $profil
     * @param string $nom
     * @param string $ville
     * @param string $dateDebut
     * @param string $dateFin
     * @param bool $formationActuelle
     */
    public function __construct(int $id, Profil $profil, string $nom, string $ville, string $dateDebut, string $dateFin, bool $formationActuelle)
    {
        $this->id = $id;
        $this->profil = $profil;
        $this->nom = $nom;
        $this->ville = $ville;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->formationActuelle = $formationActuelle;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProfil(): Profil
    {
        return $this->profil;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function getDateDebut(): string
    {
        return $this->dateDebut;
    }

    public function getDateFin(): string
    {
        return $this->dateFin;
    }

    public function isFormationActuelle(): bool
    {
        return $this->formationActuelle;
    }
}