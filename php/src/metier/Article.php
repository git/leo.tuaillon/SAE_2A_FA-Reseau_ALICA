<?php
namespace App\metier;

class Article
{
    /**
     * @var int Identifiant
     */
    private int $id;

    /**
     * @var Alumni Auteur
     */
    private Alumni $auteur;

    /**
     * @var string Sous titre de l'article
     */
    private string $sousTitre;

    /**
     * @var string Description de l'article
     */
    private string $description;

    /**
     * @param int $id
     * @param Alumni $auteur
     * @param string $sousTitre
     * @param string $description
     */
    public function __construct(int $id, Alumni $auteur, string $sousTitre, string $description)
    {
        $this->id = $id;
        $this->auteur = $auteur;
        $this->sousTitre = $sousTitre;
        $this->description = $description;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuteur(): Alumni
    {
        return $this->auteur;
    }

    public function getSousTitre(): string
    {
        return $this->sousTitre;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}