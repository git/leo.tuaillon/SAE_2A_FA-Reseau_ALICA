<?php
namespace App\metier;

class Experience
{
    /**
     * @var int Identifiant
     */
    private int $id;

    /**
     * @var Profil profil
     */
    private Profil $profil;

    /**
     * @var string Intitule
     */
    private string $intitule;

    /**
     * @var string Date début
     */
    private string $dateDebut;

    /**
     * @var string Date fin
     */
    private string $dateFin;

    /**
     * @var string Nom entreprise
     */
    private string $nomEntreprise;

    /**
     * @var bool Travail Actuel
     */
    private bool $travailActuel;

    /**
     * @param int $id
     * @param Profil $profil
     * @param string $intitule
     * @param string $dateDebut
     * @param string $dateFin
     * @param string $nomEntreprise
     * @param bool $travailActuel
     */
    public function __construct(int $id, Profil $profil, string $intitule, string $dateDebut, string $dateFin, string $nomEntreprise, bool $travailActuel)
    {
        $this->id = $id;
        $this->profil = $profil;
        $this->intitule = $intitule;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->nomEntreprise = $nomEntreprise;
        $this->travailActuel = $travailActuel;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProfil(): Profil
    {
        return $this->profil;
    }

    public function getIntitule(): string
    {
        return $this->intitule;
    }

    public function getDateDebut(): string
    {
        return $this->dateDebut;
    }

    public function getDateFin(): string
    {
        return $this->dateFin;
    }

    public function getNomEntreprise(): string
    {
        return $this->nomEntreprise;
    }

    public function isTravailActuel(): bool
    {
        return $this->travailActuel;
    }
}