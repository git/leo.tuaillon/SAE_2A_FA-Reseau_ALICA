<?php

namespace App\metier;

class Alumni{
    private int $id;
    /**
     * @var string Email
     */
    private string $email;

    /**
     * @var string Mot de passe
     */
    private string $motDePasse;

    /**
     * @var String Role
     */
    private String $role;

    private Profil $profil;

    /**
     * @param string $email
     * @param string $motDePasse
     * @param String $role
     * @param Profil $profil
     */

    public function __construct(int $id,string $email, string $motDePasse, String $role, string $nom, string $prenom)
    {
        $this->id = $id;
        $this->email = $email;
        $this->motDePasse = $motDePasse;
        $this->role = $role;
        $this->profil = new Profil($id,$nom, $prenom, $email, "","", "", "","");
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    public function getProfil() : Profil
    {
        return $this->profil;
    }
    /**
     * @return string
     */
    public function getMotDePasse(): string
    {
        return $this->motDePasse;
    }

    public function getRole(): String
    {
        return $this->role;
    }


    public function getPassword(): string
    {
        return $this->motDePasse;
    }

    public function getPrenom()
    {
        return $this->profil->getPrenom();
    }

    public function getNom()
    {
        return $this->profil->getNom();
    }

    public function getId()
    {
        return $this->id;
    }
}