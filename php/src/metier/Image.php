<?php

namespace App\metier;

class Image
{
    private int $id;
    private string $name;

    private string $taille;

    private string $type;

    private string $blob;

    /**
     * @param int $id
     * @param string $name
     * @param string $taille
     * @param string $type
     * @param string $blob
     */
    public function __construct(int $id, string $name, string $taille, string $type, string $blob)
    {
        $this->id = $id;
        $this->name = $name;
        $this->taille = $taille;
        $this->type = $type;
        $this->blob = $blob;
    }

    public function getName(): string
    {
        return $this->name;
    }


    public function getTaille(): string
    {
        return $this->taille;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getBlob(): string
    {
        return $this->blob;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function toString() : string {
        return "Image : " . $this->name . " " . $this->taille . " " . $this->type . " blob " . $this->blob;
    }


    }