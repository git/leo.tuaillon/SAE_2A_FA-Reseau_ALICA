<?php
namespace App\metier;

class Profil
{
    /**
     * @var int Identifiant
     */
    private int $id;


    /**
     * @var string CV
     */
    private string $cv;

    /**
     * @var string Nom
     */
    private string $nom;


    /**
     * @var string Prenom
     */
    private string $prenom;

    private string $email;
    /**
     * @var string Url linkedin
     */
    //image can be null
    private ?string $image;
    private string $linkedinUrl;

    /**
     * @var string Url github
     */
    private string $githubUrl;

    /**
     * @var string Url du portfolio
     */
    private string $portfolioUrl;

    /**
     * @param string $cv
     * @param string $nom
     * @param string $prenom
     * @param string $linkedinUrl
     * @param string $githubUrl
     * @param string $portfolioUrl
     */
    public function __construct(int $alumni,string $nom, string $prenom, string $email, ?string $image,string $cv, string $linkedinUrl, string $githubUrl, string $portfolioUrl)
    {
        $this->id = $alumni;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->image = $image;
        $this->email = $email;
        $this->cv = $cv;
        $this->linkedinUrl = $linkedinUrl;
        $this->githubUrl = $githubUrl;
        $this->portfolioUrl = $portfolioUrl;
    }


    public function getId(): int
    {
        return $this->id;
    }
    public function getImage(): ?string
    {
        return $this->image ?? 'logo.png';
    }
    public function getCv(): string
    {
        return $this->cv;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLinkedinUrl(): string
    {
        return $this->linkedinUrl;
    }

    public function getGithubUrl(): string
    {
        return $this->githubUrl;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
    public function getPortfolioUrl(): string
    {
        return $this->portfolioUrl;
    }
}