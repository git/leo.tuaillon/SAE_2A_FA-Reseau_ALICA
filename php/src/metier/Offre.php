<?php
namespace App\metier;

use App\metier\Alumni;
use App\metier\Profil;
use DateTime;
class TypeContrat {
    const CDI = "CDI";
    const CDD = "CDD";
    const Alternance = "Alternance";
    const Stage = "Stage";
}
//
//enum ProfilRecherche : string
//{
//    case Junior = "Junior";
//    case Senior = "Senior";
//    case Indifferent = "Indifferent";
//}
//
//enum NiveauEtudes: string
//{
//    case Bac2 = "Bac+2";
//    case Bac3 = "Bac+3";
//    case Bac5 = "Bac+5";
//    case Indifferent = "Indifferent";
//}

class Offre
{
    /**
     * @var int Identifiant
     */
    private int $id;

    /**
     * @var Alumni Offreur
     */
    private Alumni $offreur;

    /**
     * @var string intitulé de l'offre
     */
    private string $nom;

    /**
     * @var string Description de l'offre
     */
    private string $description;

    /**
     * @var string Url de l'image
     */

    /**
     * @var \App\metier\TypeContrat Type de contrat
     */
    //private \App\metier\TypeContrat $typeContrat;
    private  string $typeContrat;
    /**
     * @var string Ville
     */
    private string $ville;

    /**
     * @var string Entreprise de l'offre
     */
    private string $entreprise;

    /**
     * @var string Descriptif du poste
     */
    private string $descriptifPoste;

    /**
     * @var \App\metier\ProfilRecherche Profil recherché
     */
    //private \App\metier\ProfilRecherche $profil;
    private string $profilSearched;
    /**
     * @var string Experience
     */
    private string $experience;

    /**
     * @var \App\metier\NiveauEtudes Niveau d'études
     */
    //private \App\metier\NiveauEtudes $niveauEtudes;
    private string $niveauEtudes;

    private string $img;

    private string $logo;

    private DateTime $date;

    /**
     * @var string Email de contact
     */
    private string $mailContact;

    /**
     */
    private string $numero;

    /**
     * @var string Url du site
     */
    private string $siteUrl;

    private bool $remote;

    /**
     * @param int $id
     * @param Alumni $offreur
     * @param string $nom
     * @param string $description
     * @param string $imageUrl
     * @param \App\metier\TypeContrat $typeContrat
     * @param string $ville
     * @param string $entreprise
     * @param string $descriptifPoste
     * @param Profil $profil
     * @param string $experience
     * @param \App\metier\NiveauEtudes $niveauEtudes
     * @param string $mailContact
     * @param string $numero
     * @param string $siteUrl
     */
    public function __construct(int $id,
                                Alumni $offreur,
                                string $nom,
                                string $description,
                                string $img,
                                string $logo,
                                string $typeContrat,
                                string $ville,
                                string $entreprise,
                                string $descriptifPoste,
                                string $profil,
                                string $experience,
                                string $niveauEtudes,
                                string $mailContact,
                                string $numero,
                                string $siteUrl,
                                bool $remote,
                                DateTime $date)
    {
        $this->id = $id;
        $this->offreur = $offreur;
        $this->nom = $nom;
        $this->description = $description;
        $this->img = $img;
        $this->logo = $logo;
        $this->typeContrat = $typeContrat;
        $this->ville = $ville;
        $this->entreprise = $entreprise;
        $this->descriptifPoste = $descriptifPoste;
        $this->profilSearched = $profil;
        $this->experience = $experience;
        $this->niveauEtudes = $niveauEtudes;
        $this->mailContact = $mailContact;
        $this->numero = $numero;
        $this->siteUrl = $siteUrl;
        $this->remote = $remote;
        $this->date = $date;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOffreur(): Alumni
    {
        return $this->offreur;
    }

    public function getOffreurId() : int
    {
        return $this->offreur->getId();
    }

    public function getLogo(): string
    {
        return $this->logo;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getDescription(): string
    {
        return $this->description;
    }


    public function getImg() : string
    {
        return $this->img;
    }


    public function getTypeContrat(): string
    {
        return $this->typeContrat;
    }

    public function getVille(): string
    {
        return $this->ville;
    }

    public function getEntreprise(): string
    {
        return $this->entreprise;
    }

    public function getDescriptifPoste(): string
    {
        return $this->descriptifPoste;
    }

    public function getProfilSearched(): string
    {
        return $this->profilSearched;
    }

    public function getExperience(): string
    {
        return $this->experience;
    }

    public function getNiveauEtudes(): string
    {
        return $this->niveauEtudes;
    }

    public function getMailContact(): string
    {
        return $this->mailContact;
    }

    public function getNumero(): string
    {
        return $this->numero;
    }

    public function isRemote(): bool
    {
        return $this->remote;
    }

    public function getSiteUrl(): string
    {
        return $this->siteUrl;
    }

    public function setImgId(int $imgId): void
    {
        $this->imgId = $imgId;
    }

    public function toString() : string
    {
        return "Offre :,{$this->nom}, {$this->typeContrat},{$this->ville} {$this->entreprise}";
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function getDateString() : string
    {
        return $this->date->format('Y-m-d');
    }

    public function getDateStringFr()
    {
        return $this->date->format('d/m/Y');
    }

}