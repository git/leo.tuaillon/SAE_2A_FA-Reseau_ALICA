<?php
namespace App\metier;
use App\metier\Alumni;

class Evenement
{
    /**
     * @var int Identifiant
     */
    private int $id;

    /**
     * @var int Organisateur
     */
    private int $organisateur;

    /**
     * @var string Titre Evenement
     */
    private string $titre;

    /**
     * @var string Description de l'evenement
     */
    private string $description;

    /**
     * @var string Date de l'evenement
     */
    private string $date;

    /**
     * @var int Nombre maximal d'inscrits
     */
    private int $nbPlaceMax;

    /**
     * @var string Image de l'evenement
     */
    private string $image;

    /**
     * @param int $id
     * @param int $organisateur
     * @param string $titre
     * @param string $description
     * @param string $date
     * @param int $nbPlaceMax
     * @param string $image
     */
    public function __construct(int $id, int $organisateur, string $titre, string $description, string $date, int $nbPlaceMax, string $image)
    {
        $this->id = $id;
        $this->organisateur = $organisateur;
        $this->titre = $titre;
        $this->description = $description;
        $this->date = $date;
        $this->nbPlaceMax = $nbPlaceMax;
        $this->image = $image;

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return int 
     */
    public function getOrganisateurId(): int
    {
        return $this->organisateur;
    }

    /**
     * @return string 
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string 
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return int 
     */
    public function getNbPlaceMax(): int
    {
        return $this->nbPlaceMax;
    }

    /**
     * @return int 
     */
    public function getImage(): string
    {
        return $this->image;
    }
}