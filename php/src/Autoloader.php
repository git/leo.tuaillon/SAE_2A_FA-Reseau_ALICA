<?php
spl_autoload_register(function ($class) {
    $prefix = 'MyProject\\';
    $baseDir = __DIR__;

    $class = ltrim($class, '\\');
    $file = '';

    if (0 === strpos($class, $prefix)) {
        $class = substr($class, strlen($prefix));
        $file = $baseDir . '/' . str_replace('\\', '/', $class) . '.php';
    }

    if (file_exists($file)) {
        require $file;
    }
});
