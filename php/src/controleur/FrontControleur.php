<?php


namespace App\controleur;

use App\router\AltoRouter;
use App\controleur\Error;

class FrontControleur
{
    public function __construct()
    {
        global $twig;
        session_start();

        if(!isset($_SESSION['nom']) && !isset($_SESSION['prenom']) && !isset($_SESSION['role']) && !isset($_SESSION['id']))
        {
            $_SESSION['nom'] = NULL;
            $_SESSION['prenom'] = NULL;
            $_SESSION['role'] = "guest";
            $_SESSION['id'] = NULL;
        }
       else{
            $twig->addGlobal('nom', $_SESSION['nom']);
            $twig->addGlobal('prenom', $_SESSION['prenom']);
            $twig->addGlobal('role', $_SESSION['role']);
            $twig->addGlobal('id', $_SESSION['id']);
        }


        $router = new AltoRouter();

        $router->setBasePath('/SAE_2A_FA-Reseau_ALICA/php');

        $router->map('GET|POST', '/', 'UtilisateurControleur');

        $router->map('GET|POST','/[a:action]?','UtilisateurControleur');

        $router->map('POST','/[a:action]?','UtilisateurControleur');

        $router->map('GET|POST','/[a:action]/[i:id]?','UtilisateurControleur');

        $router->map('GET|POST', '/user/[a:action]?', 'MembreControleur');

        $router->map('GET|POST', '/user/[i:id]/[a:action]?', 'MembreControleur');

        $router->map('GET|POST', '/user/[i:id]/[a:action]/[i:id2]?', 'MembreControleur');


        $router->map('GET|POST', '/admin/[i:id]/[a:action]?', 'AdminControleur');

        $router->map('GET|POST', '/admin/[i:id]/[a:action]/[i:id2]?', 'AdminControleur');

        $id = 0;

        $match = $router->match();

        $action = array();

        $id = array();

        try{
            if (!$match) {

                $dVueErreur[] = "Error 404 Page not found";
                echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur]);
            }

            if ($match) {

                $controller = $match['target'] ?? NULL;
                $action = $match['params']['action'] ?? NULL;
                $id = $match['params']['id'] ?? NULL;

                $namespace = 'App\\controleur\\';

                try {
                    if ($controller == "MembreControleur") {
                        if ($_SESSION["role"] != "Membre" && $_SESSION["role"] != "Admin") {

                            echo $twig->render("connection.html", ['msg' => 'Vous devez vous connecter pour effectuer cette action']);
                        } else {
                            $controller = "MembreControleur";
                            $controller = $namespace . $controller;
                            //echo "controller : ".$controller;
                            $controller = new $controller();

                        }
                    }

                    if ($controller == "AdminControleur") {
                        if ($_SESSION["role"] != "Admin") {
                            $dVueErreur = ["Erreur : Vous n'avez pas les privileges pour cette action"];

                            global $twig;
                            echo $twig->render('erreur.html', ['dVueErreur' => $dVueErreur]);
                            return;
                        } else {
                            $controller = "AdminControleur";
                            $controller = $namespace . $controller;
                            $controller = new $controller();
                        }
                    }

                    if ($controller == "UtilisateurControleur") {
                        $controller = $namespace . $controller;
                        $controller = new $controller();
                    }

                    if (is_callable(array($controller, $action))) {

                        call_user_func(array($controller, $action), $match['params']);
                    } else {


                        echo $twig->render('accueil.html');
                    }

                } catch (Error $error) {
                    $dVueErreur = ['Erreur : Action inconnue'];

                    echo $twig->render('erreur.html', ['dVueErreur' => $dVueErreur]);
                }
            }
        }
        catch(Error $e)
        {
            $dVueErreur = ['Erreur : Action inconnue'];

            echo $twig->render('erreur.html', ['dVueErreur' => $dVueErreur]);
        }
}



}
