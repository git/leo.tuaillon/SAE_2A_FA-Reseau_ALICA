<?php

namespace App\controleur;
use App\config\Validation;
use App\gateway\ImageManager;
use App\modele\MembreModele;

class MembreControleur extends UtilisateurControleur
{
    public function __construct()
    {}

    public function deconnexion()
    {
        global $twig;
        session_unset();
        session_destroy();

        header("Location: /SAE_2A_FA-Reseau_ALICA/php/");
        exit();
    }
    public function createOfferForm()
    {
        global $twig;
        echo $twig->render("CreateOffer.html", []);
    }

    public function createOffer()
    {
        global $twig;
        $taberror = [];

        $requiredFields = ['name', 'entreprise', 'description','typeContrat', 'descriptPoste', 'profilRecherche', 'choixExp', 'education', 'mail', 'num'];

        $error = false;
        foreach ($requiredFields as $field) {
            if (empty($_POST[$field])) {
                $error = true;
                $taberror[] = "Le champ {$field} est requis !";
            }
        }
        if($error)
        {
            echo $twig->render("CreateOffer.html", ['tabError' => $taberror ]);
            return;
        }

        if(!Validation::verifierEmail($_POST["mail"]))
        {
            $taberror[] = "Email non valide !";
        }

        if(!Validation::validateNumber($_POST["num"]))
        {
            $taberror[] = "Numero non valide !";
        }

        if (!Validation::validateImage("image"))
        {
            $taberror[] = "Image non valide !";

        }
        if(!Validation::validateImage("logo")) {
            $taberror[] = "Logo non valide !";
        }

        if(count($taberror) > 0)
        {
            echo $twig->render("CreateOffer.html", ['tabError' => $taberror ]);

        }
        else{
            $saveImg1 = ImageManager::SaveImage("image");
            $saveImg2 = ImageManager::SaveImage("logo");
            if($saveImg1[0] != NULL &&  $saveImg2[0] != NULL)
            {
                $mbrModel = new MembreModele();

                $offre = $mbrModel->publishOffer($saveImg1[1], $saveImg2[1]);

                echo $twig->render("OffreDetail.html", ['offre' => $offre]);
            }
            else
            {
                $taberror[] = "Erreur lors de l'upload des images";
                echo $twig->render("CreateOffer.html", ['tabError' => $taberror ]);
            }
        }
    }


    public function deleteOffer(?array $params)
    {
        global $twig;
        $id = $params['id2'];

        $MemberModel = new MembreModele();
        $offre = $MemberModel->getOfferFromId($id);
        if($offre != NULL)
        {
            $MemberModel->deleteOffer($offre);

        }
        $this->consultOffers(NULL);

    }

    public function proposerOffre()
    {
        //TODO
    }
    public function consulterProfil()
    {
        //TODO
    }
    public function modifierProfil()
    {
        //TODO
    }
    public function signaler()
    {
        //TODO
    }
}