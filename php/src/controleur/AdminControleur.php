<?php

namespace App\controleur;
use App\gateway\ImageManager;
use App\config\Validation;
use App\modele\AdminModele;

use Exception;

class AdminControleur extends ModerateurControleur
{
    public function supprimerCompte(?array $params)
    {
        global $twig;
        $adminModel = new AdminModele();
        $dVueErreur = [];



        if (isset($params['id'])) {
            $profilId = Validation::validerIntPossitif($params['id']);

            try{
                $adminModel->deleteAccount($profilId);
                echo $twig->render('profil.html', []);
            } catch (Exception $e) {
                $dVueErreur[] = "Erreur lors de la suppression du compte";
                echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur]);
            }
        } else {
            $dVueErreur[] = "ID du profil non spécifié.";
            echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur]);
        }
    }

    protected function consulterSignalement()
    {
        //TODO
    }

    public function creerEvenement()
    {
        global $twig;

        if (isset($_FILES["image"])) {
            $img = ImageManager::SaveImage('image');

            if (!Validation::validerEvenement($_SESSION["id"], $_POST["titre"], $_POST["description"], $_POST["date"], $_POST["nbPlaceMax"], $img[1])) {
                $mdl = new AdminModele();
                $mdl->ajouterEvenement($_SESSION["id"], $_POST["titre"], $_POST["description"], $_POST["date"], $_POST["nbPlaceMax"], $img[1]);

                $this->listerEvenement();
            } else {
                $dVueErreur[] ="Erreur lors de la création de l'évènement";
                echo $twig->render("erreur.html",['dVueErreur' => $dVueErreur]);
            }
        } else {
            echo $twig->render('creerEvenement.html', []);
        }
    }

    public function supprimerEvenement(?array $params)
    {
        $mdl = new AdminModele();
        $mdl->deleteEvenement($params["id2"]);

        $this->listerEvenement();
    }

    protected function supprimerOffre()
    {
        //TODO
    }
}
