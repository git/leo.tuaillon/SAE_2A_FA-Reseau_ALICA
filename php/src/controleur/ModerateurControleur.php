<?php

namespace App\controleur;


class ModerateurControleur extends MembreControleur
{
    public function __construct()
    {
        global $twig;
        if (!isset($_REQUEST["action"])) {
            $action = NULL;
        } else {
            $action = \App\config\Validation::nettoyerString($_REQUEST["action"]);
        }
        switch ($action) {
            default:
                parent::__construct();
        }
    }
}