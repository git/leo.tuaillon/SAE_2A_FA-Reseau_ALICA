<?php

namespace App\controleur;

use App\config\Validation;
use App\gateway\ImageManager;
use App\metier\Alumni;
use App\modele\OffreModele;
use App\modele\UtilisateurModele;
use Exception;

class UtilisateurControleur
{


    public function connection()
    {
        global $twig;

        $dVueErreur = [];
        $userModel = new UtilisateurModele();

        if (isset($_POST['email'], $_POST['password'])) {
            $email = Validation::nettoyerString($_POST['email']);
            $motDePasse = Validation::nettoyerString($_POST['password']);

            $utilisateur = $userModel->connection($email, $motDePasse);

            if ($utilisateur instanceof Alumni) {
                $_SESSION['nom'] = $utilisateur->getNom();
                $_SESSION['prenom'] = $utilisateur->getPrenom();
                $_SESSION['role'] = $utilisateur->getRole();
                $_SESSION['id'] = $utilisateur->getId();

                echo $twig->render('accueil.html',[
                    'nom' => $_SESSION['nom'],
                    'prenom' => $_SESSION['prenom'],
                    'role' => $_SESSION['role'],
                    'id' => $_SESSION['id'
                ]]);

                return;
            } else {
                $dVueErreur[] = "L'adresse email ou le mot de passe est incorrect.";
            }
        }
        echo $twig->render('connection.html', ['dVueErreur' => $dVueErreur]);
    }

    public function inscription()
    {

        global $twig;
        $dVueErreur = [];
        $userModel = new UtilisateurModele();

        if (isset($_POST['firstname'],$_POST['name'], $_POST['email'], $_POST['password'])) {
            $nom = Validation::nettoyerString($_POST['name']);
            $prenom = Validation::nettoyerString($_POST['firstname']);
            $email = Validation::nettoyerString($_POST['email']);
            $motDePasse = Validation::nettoyerString($_POST['password']);
            $hash = password_hash($motDePasse, PASSWORD_DEFAULT);
            try {
                // verification que l'email est valide et unique :
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $dVueErreur[] = "L'adresse email n'est pas valide ou est déjà utilisée.";
                } else {
                    $utilisateur = $userModel->getUtilisateurByEmail($email);
                    if ($utilisateur instanceof Alumni) {
                        $dVueErreur[] = "L'adresse email est déjà utilisée.";
                    }
                }
                $nouvelUtilisateur = $userModel->inscription($prenom, $nom, $email, $hash);

                if ($nouvelUtilisateur instanceof Alumni) {
                    echo $twig->render('inscription_success.html');
                    exit();
                }
            }
            catch (Exception $e) {
            $dVueErreur[] = "L'inscription a échoué, veuillez réessayer.";
            }
        }
        echo $twig->render('inscription.html', ['dVueErreur' => $dVueErreur]);
    }

    public function accueil()
    {
        global $twig;
        // Ajout d'un var_dump pour déboguer
        if (isset($_SESSION['utilisateur']) && $_SESSION['utilisateur'] instanceof Alumni) {
        $userModel = new UtilisateurModele();
        $evenements=$userModel->getEvenement();
        //aller sur la page d'accueil avec le nom et prenom de l'utilisateur
        echo $twig->render('accueil.html', ['evenements' => $evenements]);
        } else {
            echo $twig->render('accueil.html');
        }
    }

    public function consulterProfilLimite()
    {
        //TODO
        echo 'not implemented yet';
    }

    /**
     * @return void
     * @description afficher la liste des offres
     */
    public function resetFilters()
    {
        unset($_SESSION["niveauEtudes"]);
        unset($_SESSION["typeContrat"]);
        unset($_SESSION["experience"]);
        $this->consultOffers(null);
    }

    /**
     * @param array|null $params paramètres / filtres de la rehcerhce
     * @return void
     * @description afficher la liste des offres
     */
    public function consultOffers(?array $params)
    {
        $userMdl = new UtilisateurModele();
        global $twig;

        $niveauEtudes = NULL;
        $typeContrat =  NULL;
        $exp = NULL;
        $msg = NULL;

        $nbOffers = 5 ;

        if(isset($_POST["niveauEtudes"]) && Validation::validateTypeStudies($_POST["niveauEtudes"])) {
            //$niveauEtudes = $_POST["niveauEtudes"];
            $_SESSION["niveauEtudes"] = $_POST["niveauEtudes"];
            $niveauEtudes = $_SESSION["niveauEtudes"];
        }
        else if(isset($_SESSION["niveauEtudes"]))
        {$niveauEtudes = $_SESSION["niveauEtudes"];}

        if(isset($_POST["typeContrat"]) && Validation::validateTypeContract($_POST["typeContrat"])) {
            //$typeContrat = $_POST["typeContrat"];
            $_SESSION["typeContrat"] = $_POST["typeContrat"];
            $typeContrat = $_SESSION["typeContrat"];
        }
        else if(isset($_SESSION["typeContrat"]))
        {$typeContrat = $_SESSION["typeContrat"];}

        if(isset($_POST["experience"]) && Validation::validateExperience($_POST["experience"])) {
            //$exp = $_POST["experience"];
            $_SESSION["experience"] = $_POST["experience"];
            $exp = $_SESSION["experience"];
        }
        else if(isset($_SESSION["experience"]))
        {$exp = $_SESSION["experience"];}


        if ($niveauEtudes == null && $typeContrat == null && $exp == null) {
            $totalOffers = $userMdl->getNbOffers();
        } else {
            $params = array(
                'typeContrat' => $typeContrat,
                'exp' => $exp,
                'niveauEtudes' => $niveauEtudes
            );
            //$offers = $userMdl->getOffersWithFilters($params);
            //var_dump($offers);
            //$totalOffers = count($offers);
            $totalOffers = count($userMdl->getOffersWithFilters($params));
        }

        $numberPages = ceil($totalOffers / 5);

        //var_dump($params);
        //echo "page : ".$params["id"];
        /*if (isset($params["id"]) && intval($params["id"]) != null)*/
        if(isset($_GET['id']) && intval($_GET['id']) != null)
        {
            $page = intval($_GET['id']);

            //echo "page : ".$page;
            if ($page > $numberPages || $page < 1) {
                $dVueErreur[] = "Page introuvable";
                echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur ]);
                return;
            }
        } else {
            $page = 1;
        }

        $start = intval(($page - 1) * 5);

        $offers = [];

        if ($niveauEtudes == null && $typeContrat == null && $exp == null) {
            $offers = $userMdl->getOfferLimit($start, $nbOffers);
        } else {
            $params['start'] = $start;
            $params['nbOffers'] = 5;
            $offers = $userMdl->getOffersWithFilters($params);

        }

        echo $twig->render('OffersList.html', [
            'msg' =>   $msg,
            'offres' => $offers,
            'numberPages' => $numberPages,
            'currentPage' => $page,
            'typeContrat' => (($typeContrat != null) ? $typeContrat : ""),
            'experience' => (($exp != null) ? $exp : ""),
            'niveauEtudes' => (($niveauEtudes != null) ? $niveauEtudes : ""),
            'valContrat' => (($typeContrat != null) ? "&typeContrat=".$typeContrat : ""),
            'valExp' => (($exp != null) ? "&experience=".$exp : ""),
            'valEtudes' => (($niveauEtudes != null) ? "&niveauEtudes=".$niveauEtudes : "")
        ]);
    }

    /**
     * @param array|null $params paramètres
     * @return void
     * @description afficher le détail d'une offre
     */
    public function displayOffer(?array $params)
    {
        global $twig;

        if (isset($params['id']) && intval($params['id']) != null)
        {
            $uttilsMdl = new UtilisateurModele();
            $offre = $uttilsMdl->getOfferFromId(intval($params["id"]));
            if($offre != NULL)
            {
                echo $twig->render("OffreDetail.html",['offre' => $offre]);
                return;
            }
        }
        $dVueErreur[] = "Erreur, Offre introuvable";
        echo $twig->render("erreur.html", ['dVueErreur' => $dVueErreur]);
    }

    public function listerEvenement()
    {
        $mdl = new UtilisateurModele();
        $evenements = $mdl->getEvenement();
        global $twig;

        echo $twig->render('evenement.html', ['evenements' => $evenements]);
    }
    
    public function avoirDetailEvenement(?array $params)
    {
        $mdl = new UtilisateurModele();
        global $twig;

        if(isset($params['id']))
        {
            $evenement = $mdl->getEvenementById($params['id']);


            echo $twig->render('detailEvenement.html', ['evenement' => $evenement]);
        }
        else echo $twig->render('erreur.html', ['dVueErreur' => ['erreur id evenement']]);


    }

    public function rechercherEvenement()
    {
        $mdl = new UtilisateurModele();
        $evenements = $mdl->getEvenement();

        if (isset($_POST["recherche"]) and !empty($_POST["recherche"])) {
            $recherche = Validation::nettoyerString($_POST["recherche"]);
            $evenements = $mdl->getEvenementByTitre($recherche);
        }

        global $twig;
        echo $twig->render('evenement.html', ['evenements' => $evenements]);
    }

    public function getProfilByPage(?array $params)
    {

        global $twig;
        $dVueErreur = []; // Tableau pour stocker les erreurs, le cas échéant
        $userModel = new UtilisateurModele();
        $nbParPage = 6;
        $nombreTotalPages = ceil(($userModel->getNbTotalPages())/$nbParPage);
        if (isset($params['id'] ) && $params['id'] != null) {
            $page = Validation::validerIntPossitif($params['id']);
            try{
                $profils = $userModel->getProfilByPage($page, $nbParPage);
                if (isset($profils)) {
                    echo $twig->render('profil.html', [
                            'profils' => $profils,
                            'nombreTotalPages' => $nombreTotalPages,
                            'page' => $page]
                    );
                }
            }catch (Exception $e){
                $dVueErreur[] = "Aucun profil n'a été trouvé.";
                echo $twig->render('erreur.html', ['dVueErreur' => $dVueErreur]);
            }
        } else {
            $dVueErreur[] = "La page n'existe pas.";
            echo $twig->render('erreur.html', ['dVueErreur' => $dVueErreur]);
        }
    }
}